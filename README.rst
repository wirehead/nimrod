===============================
nimrod
===============================

.. image:: https://img.shields.io/travis/scrudgey/nimrod.svg
        :target: https://travis-ci.org/scrudgey/nimrod

.. image:: https://img.shields.io/pypi/v/nimrod.svg
        :target: https://pypi.python.org/pypi/nimrod


Package for parsing randomized text generation templates.

* Free software: BSD license
* Documentation: https://nimrod.readthedocs.org.

Features
--------

* TODO
